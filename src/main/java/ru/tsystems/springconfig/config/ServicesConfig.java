package ru.tsystems.springconfig.config;

import ru.tsystems.springconfig.dao.PersonDao;
import ru.tsystems.springconfig.service.PersonService;
import ru.tsystems.springconfig.service.PersonServiceImpl;


public class ServicesConfig {

    public PersonService personService(PersonDao dao) {
        return new PersonServiceImpl(dao);
    }
}
