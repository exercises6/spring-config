package ru.tsystems.springconfig.config;

import ru.tsystems.springconfig.dao.PersonDao;
import ru.tsystems.springconfig.dao.PersonDaoSimple;


public class DaoConfig {

    public PersonDao personDao() {
        return new PersonDaoSimple();
    }
}
