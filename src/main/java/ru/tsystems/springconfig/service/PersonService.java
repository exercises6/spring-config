package ru.tsystems.springconfig.service;

import ru.tsystems.springconfig.domain.Person;

public interface PersonService {

    Person getPerson();
}
