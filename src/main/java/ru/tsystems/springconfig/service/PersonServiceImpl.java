package ru.tsystems.springconfig.service;

import ru.tsystems.springconfig.dao.PersonDao;
import ru.tsystems.springconfig.domain.Person;

public class PersonServiceImpl implements PersonService {

    private final PersonDao dao;

    public PersonServiceImpl(PersonDao dao) {
        this.dao = dao;
    }

    public Person getPerson() {
        return dao.getPerson();
    }
}
