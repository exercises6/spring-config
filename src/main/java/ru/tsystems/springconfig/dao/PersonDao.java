package ru.tsystems.springconfig.dao;

import ru.tsystems.springconfig.domain.Person;

public interface PersonDao {

    Person getPerson();
}
