package ru.tsystems.springconfig.dao;

import ru.tsystems.springconfig.domain.Person;

public class PersonDaoSimple implements PersonDao {

    public Person getPerson() {
        return new Person("Ivan", 18);
    }
}
